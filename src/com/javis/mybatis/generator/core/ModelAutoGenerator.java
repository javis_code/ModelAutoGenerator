package com.bookstore.utils.generator.core;

import com.bookstore.utils.generator.common.Init;
import com.bookstore.utils.generator.constants.Const;
import com.bookstore.utils.generator.templates.*;
import com.bookstore.utils.generator.utils.CommonUtils;
import com.bookstore.utils.generator.utils.Config;
import com.bookstore.utils.generator.utils.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * 连接mysql自动生成模型
 * @author Javis
 */
public class ModelAutoGenerator {

	public static void generateModelFile(String path) {
		Config.loadProperties(path);
		long startTime = System.currentTimeMillis();
		try {
			generateEntity(Init.TABLES, Init.CONTAIN_DATE_TABLES, Init.COLUMNS);
			generateDaoApi(Init.TABLES);
			generateSqlMapper(Init.TABLES, Init.COLUMNS);
			generateServiceApi(Init.TABLES);
			generateService(Init.TABLES);
		} catch (IOException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("模型生成成功！耗时：" + (endTime - startTime) / 1000 + "秒");
	}

	/**
	 * 生成实体类
	 * @param tableList
	 * @param containsDateTypeTableList
	 * @param columnList
	 * @throws IOException
	 */
	public static void generateEntity(List<String> tableList, List<String> containsDateTypeTableList,
									  List<String[]> columnList) throws IOException {
		BufferedWriter bw = null;
		File file;
		boolean hasDateType = CommonUtils.checkDateTypeExists(containsDateTypeTableList, tableList);
		try {
			for (String tableName : tableList) {
				file = FileUtils.createFile(Const.SAVE_POJO_PATH, tableName);  /* 创建文件 */
				bw = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				builder.append(EntityClassTemplate.packageString()) /* package */
				       .append(EntityClassTemplate.importBaseString()); /* import */
				if (hasDateType) {
					builder.append(EntityClassTemplate.importDateString()); /* import date */
				}
				builder.append(EntityClassTemplate.classHeaderString(tableName));
				for (String[] c : columnList) { /* c[0]:表名 c[1]:字段名 c[2]:字段类型 c[3]:字段注释  */
					if (c[0].equals(tableName)) {
						if (!c[3].isEmpty()) { /* 如果字段注释长度不为0，则添加注释 */
							builder.append(EntityClassTemplate.commentString(c[3]));  /* 添加注释 */
						}
						builder.append(EntityClassTemplate.attributeString(c[1], c[2]));  /* 声明属性 */
					}
				}
				for (String[] c : columnList) {
					if (c[0].equals(tableName)) {
						builder.append(EntityClassTemplate.getMethodString(c[1], c[2])); /* get */
						builder.append(EntityClassTemplate.setMethodString(c[1], c[2])); /* set */
					}
				}
				builder.append(EntityClassTemplate.defaultConstructor(tableName)); /* 默认构造函数 */
				bw.write(builder.toString());
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	/**
	 * 生成数据访问接口
	 * @param tableList
	 * @throws IOException
	 */
	public static void generateDaoApi(List<String> tableList) throws IOException {
		BufferedWriter bw = null;
		File file;
		try {
			for (String tableName : tableList) {
				file = FileUtils.createFile(Const.SAVE_DAO_PATH, tableName);
				bw = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				builder.append(DaoTemplate.packageString());
				builder.append(DaoTemplate.importBaseString());
				builder.append(DaoTemplate.importEntityString(tableName));
				builder.append(DaoTemplate.classHeaderString(tableName));
				bw.write(builder.toString());
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	/**
	 * 生成Mapper
	 * @param tableList
	 * @throws IOException
	 */
	public static void generateSqlMapper(List<String> tableList, List<String[]> columnList) throws IOException {
		BufferedWriter bw = null;
		File file;
		try {
			for (String tableName : tableList) {
				file = FileUtils.createFile(Const.SQL_MAPPER_PATH, tableName);
				bw = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				builder.append(MapperTemplate.start(tableName));
				for (String[] c : columnList) {  /* c[0]:表名 c[1]:字段名 c[2]:字段类型 c[3]:字段注释  */
					if (c[0].equals(tableName)) {
						if (c[1].equals("id")) {
							builder.append(MapperTemplate.labelForId(tableName));
						} else {
							builder.append(MapperTemplate.labelForResult(c[1], tableName));
						}
					}
				}
				builder.append(MapperTemplate.end());
				bw.write(builder.toString());
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	/**
	 * 生成业务接口
	 * @param tableList
	 * @throws IOException
	 */
	public static void generateServiceApi(List<String> tableList) throws IOException {
		BufferedWriter bw = null;
		File file;
		try {
			for (String tableName : tableList) {
				file = FileUtils.createFile(Const.SAVE_SERVICE_API_PATH, tableName);
				bw = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				builder.append(ServiceApiTemplate.packageString());
				builder.append(ServiceApiTemplate.importBaseString());
				builder.append(ServiceApiTemplate.importEntityString(tableName));
				builder.append(ServiceApiTemplate.classHeaderString(tableName));
				bw.write(builder.toString());
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	/**
	 * 生成业务实现类
	 * @param tableList
	 * @throws IOException
	 */
	public static void generateService(List<String> tableList) throws IOException {
		BufferedWriter bw = null;
		File file;
		try {
			for (String tableName : tableList) {
				file = FileUtils.createFile(Const.SAVE_SERVICE_PATH, tableName);
				bw = new BufferedWriter(new FileWriter(file));
				StringBuilder builder = new StringBuilder();
				builder.append(ServiceTemplate.packageString());
				builder.append(ServiceTemplate.importBaseString());
				builder.append(ServiceTemplate.importServiceAnnotation());
				builder.append(ServiceTemplate.importEntityString(tableName));
				builder.append(ServiceTemplate.classHeaderString(tableName));
				bw.write(builder.toString());
				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			bw.close();
		}
	}

	/**
	 * 主方法
	 */
	public static void execute(String path) {
		ModelAutoGenerator.generateModelFile(path);
	}
	public static void main(String[] args) {
		execute("config/mybatis.properties");
	}
}
