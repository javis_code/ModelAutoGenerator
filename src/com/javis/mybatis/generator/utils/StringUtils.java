package com.bookstore.utils.generator.utils;

import com.bookstore.utils.generator.constants.Const;

public class StringUtils {

	/**
	 * 把字段类型转换成Java类型
	 * @param type
	 * @return type
	 */
	public static String convertToJavaType(String type) {
		if (type.contains("varchar")) {
			return "String";
		} else if (type.contains("date") || type.contains("datetime")) {
			return "Date";
		} else if (type.contains("text")) {
			return "String";
		} else if (type.contains("int")) {
			return "int";
		} else if (type.contains("blob")) {
			return "String";
		} else if (type.contains("double")) {
			return "double";
		}
		return null;
	}

	/**
	 * 转换成驼峰命名法
	 * @param name
	 * @param type Const.CAMEL_CASE.全部单词首字母大写 2.从第二个单词开始首字母大写
	 * @return
	 */
	public static String toCamelCase(String name, int type) {
		String[] words = name.split("_");
		StringBuilder result = type == Const.CAMEL_CASE ? new StringBuilder() : new StringBuilder(words[0]);
		for (int i = 0, len = words.length; i < len; i++) {
			if (type == Const.CAMEL_CASE) {
				if (!words[i].equals("t")) {
					result.append(words[i].substring(0, 1).toUpperCase() + words[i].substring(1, words[i].length()));
				}
			} else if (type == Const.PASCAL_CASE) {
				if (i > 0) {
					result.append(words[i].substring(0, 1).toUpperCase() + words[i].substring(1, words[i].length()));
				}
			}
		}
		return result.toString();
	}

	/**
	 * 将首字母转换为小写
	 * @param word
	 * @return
     */
	public static String toLowerCaseForInitial(String word) {
		return word.substring(0, 1).toLowerCase();
	}
}
