package com.bookstore.utils.generator.utils;

import java.util.List;

/**
 * Created by Administrator on 2017/1/12 0012.
 */
public class CommonUtils {

    /**
     * 检查当前表有没有Date类型的字段
     * @param containsDateTypeTableList
     * @param tableList
     * @return
     */
    public static boolean checkDateTypeExists(List<String> containsDateTypeTableList, List<String> tableList) {
        for (String cdTableName : containsDateTypeTableList) {
            for (String tableName : tableList) {
                if (cdTableName.equals(tableName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
