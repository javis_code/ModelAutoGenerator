package com.bookstore.utils.generator.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private static Properties properties = new Properties();
	
	/**
	 * 加载系统配置文件
	 */
	public static void loadProperties(String path) {
		try (InputStream in = Config.class.getClassLoader().getResourceAsStream(path)) {
			properties.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据获取配置文件对应的值
	 * @param key 键
	 * @return
	 */
	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
