package com.bookstore.utils.generator.utils;

import com.bookstore.utils.generator.constants.Const;

import com.javis.mybatis.generator.constants.Const;

import java.io.File;
import java.io.IOException;

/**
 * File工具类
 */
public class FileUtils {

    /**
     * 新建文件夹和新建文件
     * @param folderPath 文件所属的文件夹路径
     * @param filename 文件名称
     * @return file
     * @throws IOException
     */
	public static File createFile(String folderPath, String filename) throws IOException {
        File file = new File(folderPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        if (folderPath.equals(Const.SAVE_POJO_PATH)) {
<<<<<<< HEAD
            file = new File(folderPath + "/" + filename + "VO.java");
        } else if (folderPath.equals(Const.SAVE_DAO_PATH)) {
            file = new File(folderPath + "/I" + filename + "DAO.java");
=======
            file = new File(folderPath + "/" + filename + ".java");
        } else if (folderPath.equals(Const.SAVE_DAO_PATH)) {
            file = new File(folderPath + "/I" + filename + "Dao.java");
>>>>>>> 7e06fa3f722b5d1e0010882a0c1d46b046e7ca64
        } else if (folderPath.equals(Const.SAVE_SERVICE_PATH)) {
            file = new File(folderPath + "/" + filename + "ServiceImpl.java");
        } else if (folderPath.equals(Const.SAVE_SERVICE_API_PATH)) {
            file = new File(folderPath + "/I" + filename + "Service.java");
        } else if (folderPath.equals(Const.SQL_MAPPER_PATH)) {
<<<<<<< HEAD
            file = new File(folderPath + "/" + filename + "Mapper.xml");
=======
            file = new File(folderPath + "/" + filename + ".xml");
>>>>>>> 7e06fa3f722b5d1e0010882a0c1d46b046e7ca64
        }
        file.createNewFile();
        return file;
	}
}
