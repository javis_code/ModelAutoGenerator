package com.bookstore.utils.generator.constants;

import com.bookstore.utils.generator.utils.Config;

public class Const {

	/** 驼峰命名 */
	public static final int CAMEL_CASE = 1;
	/** 帕斯卡 */
	public static final int PASCAL_CASE = 2;

	/** Java文件 */
	public static final String FILE_TYPE_JAVA = "java";
	/** XML文件 */
	public static final String FILE_TYPE_XML = "xml";

	/** 数据库 */
	public static final String DATABASE_NAME = Config.getProperty("DATABASE_NAME");
	/** Url */
	public static final String URL = Config.getProperty("URL");
	/** 用户名 */
	public static final String USERNAME = Config.getProperty("USERNAME");
	/** 密码 */
	public static final String PASSWORD = Config.getProperty("PASSWORD");
	/** 项目名 */
	public static final String PROJECT_NAME = Config.getProperty("PROJECT_NAME");
	/** 包名 */
	public static final String PACKAGE_NAME = Config.getProperty("PACKAGE_NAME");

	/**
	 *	===========================================================
	 */
	/** Java文件存放路径 */
	public static final String JAVA_CODE_PATH = Config.getProperty("JAVA_CODE_PATH");
	/** SqlMapper文件存放路径 */
	public static final String SQL_MAPPER_PATH = Config.getProperty("SQL_MAPPER_PATH");
	/** 实体类存放位置 */
	public static final String SAVE_POJO_PATH = JAVA_CODE_PATH + "/vo";
	/** 数据访问接口存放位置 */
	public static final String SAVE_DAO_PATH = JAVA_CODE_PATH + "/dao";
	/** 业务接口存放位置 */
	public static final String SAVE_SERVICE_API_PATH = JAVA_CODE_PATH + "/service/api";
	/** 业务实现类存放位置 */
	public static final String SAVE_SERVICE_PATH = JAVA_CODE_PATH + "/service/impl";

	/**
	 *	===========================================================
	 */

	/** 查询数据库中所有的表名SQL */
	public static final String SQL_QUERY_TABLE = "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA ='DATABASE_NAME'"
			.replace("DATABASE_NAME", DATABASE_NAME);
	// public static final String SQL_QUERY_TABLE = "SELECT TABLE_NAME FROM
	// information_schema.TABLES WHERE TABLE_NAME = 'notice_message_tb'";
	/** 查询当前表的所有字段名称和类型 */
	public static final String SQL_QUERY_COLUMN = "SELECT t.TABLE_NAME,c.COLUMN_NAME,c.COLUMN_TYPE,c.COLUMN_COMMENT FROM information_schema.`TABLES` t  LEFT JOIN information_schema.`COLUMNS` c ON t.TABLE_NAME = c.TABLE_NAME WHERE t.TABLE_SCHEMA = 'DATABASE_NAME'"
			.replace("DATABASE_NAME", DATABASE_NAME);
	/** 查询字段类型有date或datetime类型的表 */
	public static final String SQL_QUERY_CONTAIN_DATETYPE_TABLE = "SELECT t.TABLE_NAME FROM information_schema.`TABLES` t  LEFT JOIN information_schema.`COLUMNS` c ON t.TABLE_NAME = c.TABLE_NAME WHERE t.TABLE_SCHEMA = 'DATABASE_NAME' AND (c.COLUMN_TYPE = 'date'	OR c.COLUMN_TYPE = 'datetime') GROUP BY	t.TABLE_NAME"
			.replace("DATABASE_NAME", DATABASE_NAME);
}
