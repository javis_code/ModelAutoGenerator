package com.bookstore.utils.generator.templates;

import com.bookstore.utils.generator.constants.Const;

/**
 * ServiceApi
 * @author Javis
 */
public class ServiceApiTemplate {

	/**
	 * package语句模板
	 * @return
     */
	public static String packageString() {
		return "package " + Const.PACKAGE_NAME + ".service.api;\r\n\r\n";
	}

	/**
	 * importBase语句模板
	 * @return
	 */
	public static String importBaseString() {
		return "import " + Const.PACKAGE_NAME + ".base.IBaseService;\r\n\r\n";
	}

	/**
	 * importEntity模板
	 * @param entityName
     * @return
     */
	public static String importEntityString(String entityName) {
		return "import " + Const.PACKAGE_NAME + ".vo." + entityName + ";\r\n\r\n";
	}

	/**
	 * 类名模板
	 * @param className
	 * @return
	 */
	public static String classHeaderString(String className) {
		return "public interface I" + className + "Service extends IBaseService<" + className + "VO> {\r\n\r\n}";
	}
}
