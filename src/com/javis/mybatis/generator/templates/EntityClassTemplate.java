package com.bookstore.utils.generator.templates;

import com.bookstore.utils.generator.constants.Const;
import com.bookstore.utils.generator.utils.StringUtils;

/**
 * 实体类模板
 * @author Javis
 */
public class EntityClassTemplate {

	/**
	 * package语句模板
	 * @return
     */
	public static String packageString() {
		return "package " + Const.PACKAGE_NAME + ".vo;\r\n\r\n";
	}

	/**
	 * import语句模板
	 * @return
	 */
	public static String importBaseString() {
		return "import " + Const.PACKAGE_NAME + ".base.BaseModel;\r\n";
	}

	/**
	 *importDate语句模板
	 * @return
	 */
	public static String importDateString() {
		return "import java.utils.Date;\r\n\r\n";
	}

	/**
	 * 类名模板
	 * @param className
	 * @return
	 */
	public static String classHeaderString(String className) {
		return "public class " + className + "VO extends BaseModel {\r\n\r\n";
	}

	/**
	 * 注释模板
	 * @param comment
	 * @return
	 */
	public static String commentString(String comment) {
		return "\t/* " + comment + " */\r\n";
	}

	/**
	 * 属性声明模板
	 * @param attribute 属性名称
	 * @param type 属性类型
	 * @return
	 */
	public static String attributeString(String attribute, String type) {
		return "\tprivate " + type + " " + StringUtils.toCamelCase(attribute, Const.PASCAL_CASE) + ";\r\n\r\n";
	}

	/**
	 * get方法模板
	 * @param attribute 属性名称
	 * @param type 属性类型
	 * @return
	 */
	public static String getMethodString(String attribute, String type) {
		return "\tpublic " + type + " get" + StringUtils.toCamelCase(attribute, Const.CAMEL_CASE) + "() {\r\n" +
			   "\t\treturn " + StringUtils.toCamelCase(attribute, Const.PASCAL_CASE) + ";\r\n\t}\r\n\r\n";
	}

	/**
	 * get方法模板
	 * @param attribute 属性名称
	 * @param type 属性类型
	 * @return
	 */
	public static String setMethodString(String attribute, String type) {
		return "\tpublic void set" + StringUtils.toCamelCase(attribute, Const.CAMEL_CASE) + "(" + type + " " + attribute + ") {\r\n" +
			   "\t\tthis." + StringUtils.toCamelCase(attribute, Const.PASCAL_CASE) + " = " + attribute + ";\r\n\t}\r\n\r\n";
	}

	/**
	 * 默认构造方法模板
	 * @param className 类名
	 * @return
	 */
	public static String defaultConstructor(String className) {
		return "\tpublic " + className + "VO() {\r\n\r\n\t}\r\n\r\n}";
	}
}
