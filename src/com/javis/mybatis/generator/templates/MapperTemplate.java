package com.bookstore.utils.generator.templates;

import com.bookstore.utils.generator.constants.Const;
import com.bookstore.utils.generator.utils.StringUtils;

/**
 * Mapper模板
 * @author Javis
 */
public class MapperTemplate {

	/**
	 * XML头模板
	 * @return
     */
	public static String start(String entityName) {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
			   "<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">\r\n" +
			   "<mapper namespace=\""+ Const.PACKAGE_NAME +".dao.I" + entityName + "DAO\">\r\n\r\n" +
			   "\t<resultMap id=\"" + entityName + "ResultMap\" type=\""+ Const.PACKAGE_NAME + ".vo." + entityName + "VO\">\r\n";
	}

	/**
	 * 结束标签模板
	 * @return
	 */
	public static String end() {
		return "\t</resultMap>\r\n</mapper>";
	}
	/**
	 * id标签模板
	 * @param attr 属性
	 * @return
	 */
	public static String labelForId(String attr) {
		String initial = StringUtils.toLowerCaseForInitial(attr);
		return "\t\t<id column=\"" + initial + "_id\" property=\"id\" />\r\n";
	}

	/**
	 * result标签模板
	 * @param attr 属性
	 * @return
	 */
	public static String labelForResult(String attr, String tableName) {
		String column = StringUtils.toLowerCaseForInitial(tableName) + "_" + attr;
		String property = StringUtils.toCamelCase(attr, Const.PASCAL_CASE);
		return "\t\t<result column=\"" + column + "\" property=\"" + property + "\" />\r\n";
	}
}
